# places-python

## Getting started
Create a .env file with environment variables for the application container in the project root directory. The .env-file must contain your Google API key with Places API enabled.

Boilerplate .env file:
```
ENV=LOCAL
DEBUG=True

STATIC_ROOT=/var/www/static/
MEDIA_ROOT=/var/www/media/

GOOGLE_API_KEY=<YOUR API KEY HERE>
```

1. Build with `docker-compose build`
2. Run locally on port 8000 with `docker-compose up`
3. Migrate database `docker-compose exec django python manage.py migrate`

The endpoint will then be accessible on:

`http://localhost:8000/location/bicycle-stores/`

also there is a fake endpoint which does not call Places API on:

`http://localhost:8000/location/fake-bicycle-stores/`

## Running tests
`docker-compose run --rm django python manage.py test --exclude-tag=integration`

NOTE: Currently only set up for local development with djangos runserver. Would need to add nginx container to run with uwsgi.
