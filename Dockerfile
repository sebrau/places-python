FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    linux-headers \
    build-base

RUN apk add --no-cache \
    postgresql-client

WORKDIR /app

ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt uwsgi
RUN apk del .build-deps

ADD uwsgi.ini /etc/uwsgi/app.ini

ADD ./places /app

# Ensure container does not start before postgres container
COPY docker/*.sh /scripts/
RUN chmod +x /scripts/wait-for-postgres.sh

EXPOSE 3030 8000

CMD ["uwsgi", "--ini", "/etc/uwsgi/app.ini"]
