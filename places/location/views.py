import time

import googlemaps
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.response import Response
from rest_framework.views import APIView

from location.tests import fake_places_nearby
from . import constants
from . import serializers


class BicycleStoresView(APIView):
    @method_decorator(cache_page(60*60*24*7))
    def get(self, request):
        gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)

        page = gmaps.places_nearby(
            location=constants.SERGELS_TORG_GEOCODE,
            radius=constants.DEFAULT_RADIUS,
            type='bicycle_store'
        )
        res = page['results']
        while 'next_page_token' in page:
            time.sleep(3)  # Sleep since next_page_token wont work else
            page = gmaps.places_nearby(page_token=page['next_page_token'])
            res.extend(page['results'])

        serializer = serializers.BicycleStoreSerializer(res, many=True)
        return Response(serializer.data)


class FakeBicycleStoresView(APIView):
    @method_decorator(cache_page(60*60*24*7))
    def get(self, request):
        page = fake_places_nearby(
            location=constants.SERGELS_TORG_GEOCODE,
            radius=constants.DEFAULT_RADIUS,
            type='bicycle_store'
        )
        res = page['results']
        while 'next_page_token' in page:
            time.sleep(3)  # Sleep since next_page_token wont work else
            page = fake_places_nearby(page_token=page['next_page_token'])
            res.extend(page['results'])

        serializer = serializers.BicycleStoreSerializer(res, many=True)
        return Response(serializer.data)
