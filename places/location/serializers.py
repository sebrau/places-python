from rest_framework import serializers


class BicycleStoreSerializer(serializers.Serializer):
    vicinity = serializers.CharField()
    name = serializers.CharField()

