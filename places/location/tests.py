from unittest.mock import patch, Mock

from django.test import tag
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from . import dummy_responses


def fake_places_nearby(location=None, radius=None, type=None, page_token=None):
    if page_token is None:
        return dummy_responses.expected_first
    else:
        return dummy_responses.expected_second


class BicycleStoresViewTests(APITestCase):
    @patch('googlemaps.Client')
    def test_list(self, maps_mock):
        instance_mock = maps_mock.return_value
        instance_mock.places_nearby = Mock(side_effect=fake_places_nearby)
        url = reverse('location:bicycle-stores')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for x, y in zip(response.data, dummy_responses.final_list):
            self.assertDictEqual(x, y)
        maps_mock.assert_called_once()
        self.assertEqual(instance_mock.places_nearby.call_count, 2)

    def tearDown(self):
        from django_redis import get_redis_connection
        get_redis_connection("default").flushall()


@tag('integration')
class BicycleStoresViewIntegrationTests(APITestCase):
    def test_list(self):
        url = reverse('location:bicycle-stores')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
