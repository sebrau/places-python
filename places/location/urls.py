from django.urls import path

from . import views

app_name = 'location'

urlpatterns = [
    path('bicycle-stores/',
         views.BicycleStoresView.as_view(),
         name='bicycle-stores'),
    path('fake-bicycle-stores/',
         views.FakeBicycleStoresView.as_view(),
         name='fake-bicycle-stores'),
    ]
